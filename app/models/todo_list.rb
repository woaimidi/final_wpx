class TodoList < ApplicationRecord
	belongs_to :user

	validates :description, presence: true
	validate :date_cannot_be_in_the_past
	# validates :status, presence: true

  	def date_cannot_be_in_the_past
	    errors.add(:date, "can't be in the past") if
	      !date.blank? and date < Date.today
  	end


	scope :completed, -> {
		where(:status => true)
	}

	scope :todo, -> {
		where(:status => false)
	}

	scope :having_date_between, ->(start_date, end_date) { where(date: start_date..end_date) }
end
