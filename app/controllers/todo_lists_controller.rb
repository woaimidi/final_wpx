class TodoListsController < ApplicationController
  helper_method :date_from_parameters
  before_action :authenticate_user! , only: [:new]
  before_action :set_todo_list, only: [:show, :edit, :update, :destroy]

  # GET /todo_lists
  # GET /todo_lists.json
  def index
    if params[:search] && params[:search][:date].present?
      start_date, end_date = params[:search][:date].split(' - ')
      @todo_lists = TodoList.having_date_between(start_date, end_date)
    else
      @todo_lists = TodoList.all.order("date ASC")
    # @todo_lists = TodoList.all
    end
  end

  def date_from_parameters
    if params[:search].present?
      params[:search][:date]
    end
  end

  # GET /todo_lists/1
  # GET /todo_lists/1.json
  def show
  end

  # GET /todo_lists/new
  def new
    @todo_list = current_user.todo_lists.build
    # @todo_list = TodoList.new
  end

  # GET /todo_lists/1/edit
  def edit
  end

  def complete

    # todo_lists.update_attribute(:status, true)
    
    redirect_to todo_lists_url, notice: 'Todo item completed.'
  end

  # POST /todo_lists
  # POST /todo_lists.json
  def create
    @todo_list = current_user.todo_lists.build(todo_list_params)

    respond_to do |format|
      if @todo_list.save
        format.html { redirect_to todo_lists_url, notice: 'Todo list was successfully created.' }
        # format.html { redirect_to @todo_list, notice: 'Todo list was successfully created.' }
        format.json { render :show, status: :created, location: @todo_list }
      else
        format.html { render :new }
        format.json { render json: @todo_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /todo_lists/1
  # PATCH/PUT /todo_lists/1.json
  def update
    respond_to do |format|
      if @todo_list.update(todo_list_params)
        # format.html { redirect_to @todo_list, notice: 'Todo list was successfully updated.' }
        format.html { redirect_to todo_lists_url, notice: 'Todo list was successfully updated.' }
        format.json { render :show, status: :ok, location: @todo_list }
      else
        format.html { render :edit }
        format.json { render json: @todo_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /todo_lists/1
  # DELETE /todo_lists/1.json
  def destroy
    @todo_list.destroy
    respond_to do |format|
      format.html { redirect_to todo_lists_url, notice: 'Todo list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_todo_list
      @todo_list = TodoList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def todo_list_params
      params.require(:todo_list).permit(:title, :description, :date, :status)
    end
end
