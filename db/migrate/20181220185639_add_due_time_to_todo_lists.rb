class AddDueTimeToTodoLists < ActiveRecord::Migration[5.2]
  def change
    add_column :todo_lists, :due_time, :datetime
  end
end
