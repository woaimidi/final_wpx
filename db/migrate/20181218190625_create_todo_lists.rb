class CreateTodoLists < ActiveRecord::Migration[5.2]
  def change
    create_table :todo_lists do |t|
      t.string :title
      t.text :description
      t.date :due_time
      t.boolean :status

      t.timestamps
    end
  end
end
