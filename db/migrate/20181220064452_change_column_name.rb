class ChangeColumnName < ActiveRecord::Migration[5.2]
  def change
  	rename_column :todo_lists, :due_time, :date
  end
end
